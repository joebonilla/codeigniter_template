<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Welcome</title>
    <link rel="stylesheet" href="<?php echo base_url() . build_url('css/app.css') ?>">
</head>
<body>
<nav class="navbar navbar-fixed-top navbar-light bg-faded">
    <div class="container">

        <div class="col-md-6 col-xs-12 col-sm-12">
            <a class="" href="index.html">
                <img src="<?php echo base_url();?>assets/img/masthead1_0.png" alt="" class="img-fluid"
                     style="padding-top: 10px;width: 50%;margin: 0 auto;">
            </a>
        </div>
        <div class="col-md-6 col-xs-12 col-sm-12">
            <a class="" href="index.html"><img src="<?php echo base_url();?>assets/img/wo_logo_blue.png" alt="" class="img-fluid m-x-auto"
                                               style="width: 25%;">
            </a>
        </div>
    </div>
</nav>

<div class="container">
    <div class="col-md-12">
        <h4>Thanks for joining us. <br><br>

            Please watch these videos while waiting.</h4>
    </div>
    <div class="col-md-12 p-t-3">
        <a href="option1.html" class="btn btn-primary btn-lg pull-md-right pull-sm-right pull-xs-right m-b-4">Start</a>
    </div>
    <div class="col-md-12 m-t-3">
        If you're interested in signing-up for Google Academy training or updates, please fill out this <a
            href="http://goo.gl/forms/nFaYwRV0t8"
            class="btn-link">form</a>.
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() . build_url('js/all.js') ?>"></script>
</body>
</html>