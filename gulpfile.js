var elixir = require('laravel-elixir');
elixir.config.autoprefix = true;
elixir.config.production = true;
elixir.config.sourcemaps = true;
elixir.config.assetsPath = "resources";
elixir.config.publicPath = "assets";
elixir.config.versioning.buildFolder = "/";

elixir(function (mix) {
    mix
        //.copy('bower_components/bootstrap/sass/', 'resources/sass/')
        //.copy('bower_components/tether/src/css', 'resources/sass/tether')
        //.copy('bower_components/bootstrap/dist/js/bootstrap.min.js', 'resources/js')
        //.copy('bower_components/jquery/dist/jquery.min.js', 'resources/js')
        //.copy('bower_components/tether/dist/js/tether.min.js', 'resources/js')
        .scripts([
            'modernizr-2.8.3.min.js',
            'jquery.min.js',
            'tether.min.js',
            'bootstrap.min.js'
        ])
        .sass('app.scss')
        .version(['css/app.css','js/all.js']);
});